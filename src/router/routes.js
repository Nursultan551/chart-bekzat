
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/IndexPage.vue'), name: 'home' },
      { path: 'task2', component: () => import('pages/TaskTwo.vue'), name: 'task2' },
      { path: 'task1', component: () => import('pages/TaskOne.vue'), name: 'task1' },
      { path: 'task3', component: () => import('pages/TaskThree.vue'), name: 'task3' },
      { path: 'task4', component: () => import('pages/TaskFour.vue'), name: 'task4' },
      { path: 'task5', component: () => import('pages/TaskFive.vue'), name: 'task5' },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
